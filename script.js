const board = Array(9).fill(null);
let currentPlayer = 'X';
const winningCombinations = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6]
];

function makeMove(cell, index) {
    if (board[index] || isGameOver()) {
        return;
    }
    board[index] = currentPlayer;
    cell.innerText = currentPlayer;
    if (checkWin()) {
        alert(`${currentPlayer} wins!`);
        return;
    }
    if (board.every(cell => cell !== null)) {
        alert("It's a tie!");
        return;
    }
    currentPlayer = currentPlayer === 'X' ? 'O' : 'X';
}

function checkWin() {
    return winningCombinations.some(combination => {
        return combination.every(index => {
            return board[index] === currentPlayer;
        });
    });
}

function isGameOver() {
    return checkWin() || board.every(cell => cell !== null);
}

function resetGame() {
    board.fill(null);
    document.querySelectorAll('.cell').forEach(cell => cell.innerText = '');
    currentPlayer = 'X';
}

window.resetGame = resetGame;
window.makeMove = makeMove;
